Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    post 'user', action: :adduser , controller: :users
    get 'users' ,action: :getusers , controller: :users
    get 'user' , action: :showuser , controller: :users
    put 'user' ,  action: :updateuser , controller: :users
    delete 'user' , action: :deleteuser , controller: :users
    get 'typeahead' , action: :searchuser , controller: :users
  end
end
