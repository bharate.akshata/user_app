class Api::UsersController < ApplicationController
    before_action :getuser , only: [:showuser , :updateuser , :deleteuser]

    def adduser
        user = User.new(userparams)
        if user.save
            render json: user , status: :ok
        
        else
            render json: {msg: "user not added"} , status: :unproccessable_entity
        end
    end

    def getusers
        user = User.all 
         if user
            render json: user , status: :ok
         else
            render json: {msg: "no users found"} , status: :unproccessable_entity
         end
    end

    def showuser
        
         if @user
            render json: @user, status: :ok
         else
            render json: {msg: "user not found"} , status: :unproccessable_entity
         end
    end

    def updateuser
        
        if @user
            if @user.update(userparams)
                render json: @user , status: :ok
            else
                render json: {msg: "user not updated"} , status: :unproccessable_entity
            end
        else
            render json: {msg: "user not found"} , status: :unproccessable_entity
        end
    end

    def deleteuser
        
        if @user
            if @user.destroy()
                render json: {msg: "user deleted"} , status: :ok

            else
                render json: {msg: "user not deleted"} , status: :unproccessable_entity
            end
        else
            render json: {msg: "user not found"} , status: :unproccessable_entity
        end

    end

    def searchuser
        @parameter = params[:input]
        @results = User.or({firstName: /#{@parameter}/i} , {lastName: /#{@parameter}/i} , {email: /#{@parameter}/i})
        render json: @results , status: :ok
    end

    private

        def getuser
            @user = User.find(params[:id])
        end

        def userparams
            params.permit(:firstName , :lastName , :email)
        end
end
